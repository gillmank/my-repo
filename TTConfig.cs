/*******************************************************************************
 * Copyright 2014 Seapine Software, Inc. All rights reserved.
 * 
 * IMPORTANT:  This Software is supplied to you by Seapine Software, Inc. (�Seapine�)
 * in consideration of your agreement to the following terms, and your use, installation,
 * modification or redistribution of this Software constitutes acceptance of these terms.
 * If you do not agree with these terms, please do not use, install, modify or redistribute
 * this Software.
 * 
 * In consideration of your agreement to abide by the following terms, and subject to these
 * terms, Seapine grants you a personal, non-exclusive license, under Seapine copyrights in
 * this original Seapine software (the �Software�), to use, reproduce, modify and redistribute
 * the Software, with or without modifications, in source and/or binary forms; provided that
 * if you redistribute the Software in its entirety and without modifications, you must retain
 * this notice and the following text and disclaimers in all such redistributions of the Software.
 * Neither the name, trademarks, service marks or logos of Seapine, Inc. may be used to endorse
 * or promote products derived from the Software without specific prior written permission from
 * Seapine.  Except as expressly stated in this notice, no other rights or licenses, express or
 * implied, are granted by Seapine herein, including but not limited to any patent rights that
 * may be infringed by your derivative works or by other works in which the Software may be
 * incorporated.
 * 
 * The Software is provided by Seapine on an "AS IS" basis.  SEAPINE MAKES NO WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, REGARDING THE SOFTWARE OR ITS USE AND
 * OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS. 
 * 
 * IN NO EVENT SHALL SEAPINE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE,
 * REPRODUCTION, MODIFICATION AND/OR DISTRIBUTION OF THE SOFTWARE, HOWEVER CAUSED AND WHETHER
 * UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN
 * IF SEAPINE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace HP2TT_SPOK
{
    class TTConfig
    {
        public string LastErrorMsg = "";
        XmlDocument ConfigFile = null;
        public Boolean VerboseLog = false;
        //private const string ConfigFileName = ".\\BCACreate.xml";

        // Make sure config file exists
        public bool Load(string ConfigFileName)
        {
            FileInfo configFile = new FileInfo(ConfigFileName);
            if (configFile.Exists)
            {
                // Load the config file into memory
                ConfigFile = new XmlDocument();
                ConfigFile.Load(ConfigFileName);
                return (true);
            }
            else
            {
                return (false);
            }
        }

        // Read a value from the config file
        public string getConfigParam(string ElementName)
        {
            XmlNode mCurrentNode;

            mCurrentNode = ConfigFile.DocumentElement;
            string ElementValue = "";
            try
            {
                mCurrentNode = mCurrentNode.SelectSingleNode(ElementName);
                ElementValue = mCurrentNode.InnerText;
            }
            catch (Exception f)
            {
                LastErrorMsg = "Error retrieving the Element Value from the configuration file. \n  " + f.Message;
            }
            return (ElementValue);
        }
  
        // Read a multi-line value and return an array of strings
        public string[] getConfigArray(string ElementName)
        {
            XmlNode mCurrentNode;

            mCurrentNode = ConfigFile.DocumentElement;
            string ElementValue = "";
            try
            {
                mCurrentNode = mCurrentNode.SelectSingleNode(ElementName);
                ElementValue = mCurrentNode.InnerText;
            }
            catch (Exception f)
            {
                LastErrorMsg = "Error retrieving the Element Value from the configuration file. \n  " + f.Message;
            }

            if (ElementValue == null)
            {
                return (null);
            }
            ElementValue = ElementValue.Replace("\t", "").Replace("\n", "");
            ElementValue = ElementValue.Trim();
            string[] FieldMapArray = ElementValue.Split('\r');
            return (FieldMapArray);
        }


        // Read a multi-line value and return an array of strings
        public string[] getConfigXML(string Header, string ElementName)
        {
            XmlNode mCurrentNode;

            mCurrentNode = ConfigFile.DocumentElement;
            string ElementValue = "";
            try
            {
                mCurrentNode = mCurrentNode.SelectSingleNode(Header);
                mCurrentNode = mCurrentNode.SelectSingleNode(ElementName);
                ElementValue = mCurrentNode.InnerXml;
            }
            catch (Exception f)
            {
                LastErrorMsg = "Error retrieving the Element XML from the configuration file. \n  " + f.Message;
            }

            if (ElementValue.Length == 0)
            {
                return (null);
            }
            ElementValue = ElementValue.Replace("\t", "").Replace("\n", "").Replace("\r", "^");
            ElementValue = ElementValue.Trim();
            String XMLValue = "";
            for (int StartPos = 0; StartPos >= 0; StartPos = ElementValue.IndexOf("<", StartPos))
            {
                StartPos++; // Lead tag start
                int EndPos = ElementValue.IndexOf(">", StartPos);  // Lead tag end
                String XMLParam = ElementValue.Substring(StartPos, EndPos - StartPos);
                StartPos = EndPos += 1;  // Data start
                EndPos = ElementValue.IndexOf("<", StartPos);  // Tail tag start
                if (XMLValue.Length > 0)
                {
                    XMLValue += "\r";
                }
                XMLValue += String.Format("{0}={1}", XMLParam, ElementValue.Substring(StartPos, EndPos - StartPos));
                StartPos = EndPos + 2;  //Next search past tail tag
            }

            string[] FieldMapArray = XMLValue.Split('\r');
            return (FieldMapArray);
        }
    }
}
