/*******************************************************************************
 * Copyright 2014 Seapine Software, Inc. All rights reserved.
 * 
 * IMPORTANT:  This Software is supplied to you by Seapine Software, Inc. (�Seapine�)
 * in consideration of your agreement to the following terms, and your use, installation,
 * modification or redistribution of this Software constitutes acceptance of these terms.
 * If you do not agree with these terms, please do not use, install, modify or redistribute
 * this Software.
 * 
 * In consideration of your agreement to abide by the following terms, and subject to these
 * terms, Seapine grants you a personal, non-exclusive license, under Seapine copyrights in
 * this original Seapine software (the �Software�), to use, reproduce, modify and redistribute
 * the Software, with or without modifications, in source and/or binary forms; provided that
 * if you redistribute the Software in its entirety and without modifications, you must retain
 * this notice and the following text and disclaimers in all such redistributions of the Software.
 * Neither the name, trademarks, service marks or logos of Seapine, Inc. may be used to endorse
 * or promote products derived from the Software without specific prior written permission from
 * Seapine.  Except as expressly stated in this notice, no other rights or licenses, express or
 * implied, are granted by Seapine herein, including but not limited to any patent rights that
 * may be infringed by your derivative works or by other works in which the Software may be
 * incorporated.
 * 
 * The Software is provided by Seapine on an "AS IS" basis.  SEAPINE MAKES NO WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, REGARDING THE SOFTWARE OR ITS USE AND
 * OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS. 
 * 
 * IN NO EVENT SHALL SEAPINE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE,
 * REPRODUCTION, MODIFICATION AND/OR DISTRIBUTION OF THE SOFTWARE, HOWEVER CAUSED AND WHETHER
 * UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN
 * IF SEAPINE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HP2TT_SPOK
{
    class TTLog
    {
        protected StreamWriter LogFile;

        // Open the log file in Append mode.
        public bool Open(string FileName)
        {
            try
            {
                FileStream fs = new FileStream(FileName, FileMode.Append, FileAccess.Write, FileShare.None);
                LogFile = new StreamWriter(fs);
                LogFile.AutoFlush = true;
            }
            catch (Exception p_Exception)
            {
                Console.WriteLine("Error: Unable to open log file.  " + p_Exception.Message);
                return (true);
            }
            return (false);
        }

        // Write a new line in the log file.  All lines are prepended with the system date/time.
        public bool Write(Boolean VerboseLog, string TextToAdd)
        {
            // If the Verbose switch is not set, do not record this message.
            if (!VerboseLog)
            {
                return (false);
            }

            try
            {
                System.DateTime LogTime = System.DateTime.Now;
                LogFile.WriteLine("{0} - {1}", LogTime,TextToAdd);
            }
            catch (Exception p_Exception)
            {
                Console.WriteLine("Error: Unable to write to log file.  " + p_Exception.Message);
                return (true);
            }
            return (false);
        }

        // Flush all data from the buffer and close the log file.
        public void Close()
        {
            if (LogFile != null)
            {
                LogFile.Flush();
                LogFile.Close();
            }
        }
    }
}
